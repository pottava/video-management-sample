import datetime
import json
import os
from typing import Dict

import sqlalchemy
from middleware import log
from sqlalchemy.orm import close_all_sessions

db = None


def get_cred_config() -> Dict[str, str]:
    secret = os.environ.get("CLOUD_SQL_CREDENTIALS_SECRET")
    if secret:
        return json.loads(secret)

    if "DB_USER" not in os.environ:
        raise Exception("DB_USER needs to be set.")

    if "DB_PASSWORD" not in os.environ:
        raise Exception("DB_PASSWORD needs to be set.")

    if "DB_NAME" not in os.environ:
        raise Exception("DB_NAME needs to be set.")

    return {
        "DB_USER": os.environ["DB_USER"],
        "DB_PASSWORD": os.environ["DB_PASSWORD"],
        "DB_NAME": os.environ["DB_NAME"],
        "DB_HOST": os.environ.get("DB_HOST", None),
        "DB_CONN": os.environ.get("CLOUD_SQL_CONNECTION_NAME", None),
    }


def init_connection() -> Dict[str, int]:
    creds = get_cred_config()
    config = {
        "pool_size": 5,
        "max_overflow": 2,
        "pool_timeout": 30,
        "pool_recycle": 1800,
    }
    if creds["DB_HOST"]:
        return init_tcp_connection(creds, config)
    else:
        return init_unix_connection(creds, config)


def init_tcp_connection(
    creds: Dict[str, str], config: Dict[str, int]
) -> sqlalchemy.engine.base.Engine:
    host_args = creds["DB_HOST"].split(":")
    pool = sqlalchemy.create_engine(
        sqlalchemy.engine.url.URL.create(
            drivername="postgresql+pg8000",
            username=creds["DB_USER"],
            password=creds["DB_PASSWORD"],
            host=host_args[0],
            port=int(host_args[1]),
            database=creds["DB_NAME"],
        ),
        **config,
    )
    pool.dialect.description_encoding = None
    log.info("Database engine initialised from tcp connection")
    return pool


def init_unix_connection(
    creds: Dict[str, str], config: Dict[str, int]
) -> sqlalchemy.engine.base.Engine:
    pool = sqlalchemy.create_engine(
        sqlalchemy.engine.url.URL.create(
            drivername="postgresql+pg8000",
            username=creds["DB_USER"],
            password=creds["DB_PASSWORD"],
            database=creds["DB_NAME"],
            query={
                "unix_sock": "{}/{}/.s.PGSQL.5432".format(
                    creds.get("DB_SOCKET_DIR", "/cloudsql"),
                    creds["DB_CONN"],
                )
            },
        ),
        **config,
    )
    pool.dialect.description_encoding = None
    log.info("Database engine initialised from unix conection")
    return pool


def init_db():
    global db
    db = init_connection()


def get_objects() -> Dict:
    objects = []
    with db.connect() as conn:
        recent_votes = conn.execute(
            "SELECT uri, publish_at FROM objects " "ORDER BY publish_at DESC"
        ).fetchall()
        for row in recent_votes:
            objects.append({"uri": row[0], "publish_at": row[1]})
    return {"objects": objects}


def save_object(uri: str, publish_at: datetime):
    stmt = sqlalchemy.text(
        "INSERT INTO objects (uri, publish_at) VALUES (:uri, :publish_at)"
    )
    with db.connect() as conn:
        conn.execute(stmt, uri=uri, publish_at=publish_at)


def shutdown():
    close_all_sessions()
    log.info("All sessions closed.")
    if db:
        db.dispose()
