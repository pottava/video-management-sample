import base64
import datetime
import json
import os
import signal
import sys

import database
import uvicorn
from fastapi import FastAPI, HTTPException, Request
from middleware import jwt_authenticated, log

app = FastAPI()


@app.on_event("startup")
async def startup():
    database.init_db()


@app.get("/")
def index():
    revision = os.environ.get("K_REVISION", "local")
    return "Revision: {}".format(revision)


@app.post("/messages", status_code=201)
@jwt_authenticated
async def messages(request: Request):
    envelope = json.loads(await request.body())
    payload = base64.b64decode(envelope["message"]["data"]).decode("utf-8")
    try:
        database.save_object(
            payload,
            datetime.datetime.strptime(
                envelope["message"]["publish_time"], "%Y-%m-%dT%H:%M:%S.%fZ"
            ),
        )
    except Exception as e:
        log.exception(e)
        raise HTTPException(status_code=500, detail=f"Failed: {e}")
    log.info(f"Payload: {payload}")
    return "OK"


# @app.on_event("shutdown")
def shutdown():
    log.info("Signal received, safely shutting down.")
    database.shutdown()
    sys.exit(0)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, shutdown)
    uvicorn.run(app, host="0.0.0.0", port=int(os.environ.get("PORT", "8080")))
else:
    signal.signal(signal.SIGTERM, shutdown)
