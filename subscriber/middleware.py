import os
from functools import wraps
from logging import getLogger
from typing import Callable, TypeVar

from fastapi import HTTPException, Request
from google.auth.transport import requests
from google.oauth2 import id_token

log = getLogger("uvicorn")
a = TypeVar("a")


def jwt_authenticated(func: Callable[..., int]) -> Callable[..., int]:
    @wraps(func)
    async def decorated_function(request: Request, *args: a, **kwargs: a) -> a:

        header = request.headers.get("Authorization")
        if header:
            try:
                id_token.verify_oauth2_token(
                    header.split(" ")[1],
                    requests.Request(),
                    audience=os.environ.get("TARGET_URL", "example.com"),
                )
            except Exception as e:
                log.exception(f"Authorization: {header}")
                raise HTTPException(status_code=401, detail=f"Exception: {e}")
        return await func(request, *args, **kwargs)

    return decorated_function
