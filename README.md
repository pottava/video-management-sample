# Pub/Sub sample

## Google Cloud services

```sh
gcloud services enable compute.googleapis.com iamcredentials.googleapis.com \
    cloudresourcemanager.googleapis.com cloudbuild.googleapis.com \
    artifactregistry.googleapis.com container.googleapis.com run.googleapis.com \
    containerscanning.googleapis.com secretmanager.googleapis.com \
    vpcaccess.googleapis.com servicenetworking.googleapis.com \
    sqladmin.googleapis.com pubsub.googleapis.com
```

### Networking

```sh
gcloud compute networks create dev --subnet-mode "custom"
gcloud compute networks subnets create dev-singapore --network "dev" \
    --region "asia-southeast1" --range "10.0.0.0/24" \
    --enable-private-ip-google-access
gcloud compute firewall-rules create dev-allow-custom --network "dev" \
    --priority 65534 --action "ALLOW" --direction "INGRESS" \
    --rules "all" --source-ranges "10.0.0.0/24"
gcloud compute firewall-rules create dev-allow-iap --network "dev" \
    --priority 1000 --action "ALLOW" --direction "INGRESS" \
    --rules "tcp:22,icmp" --source-ranges "35.235.240.0/20"
```

[Configure private services access](https://cloud.google.com/vpc/docs/configure-private-services-access) for a bastion server.

```sh
gcloud compute addresses create dev-vpc-peering \
    --global --network "dev" --prefix-length 16 \
    --purpose "VPC_PEERING"
gcloud services vpc-peerings connect \
    --network "dev" --ranges "dev-vpc-peering" \
    --service "servicenetworking.googleapis.com"
```

[Configure Serverless VPC Access connectors](https://cloud.google.com/run/docs/configuring/connecting-vpc) for Cloud Run services.

```sh
gcloud compute networks vpc-access connectors create dev-vpc-access \
    --network "dev" --region "asia-southeast1" --range "10.8.0.0/28" \
    --machine-type "f1-micro" --min-instances 2 --max-instances 3
```

### PostgreSQL (Cloud SQL)

```sh
gcloud sql instances create dev-postgres \
    --database-version "POSTGRES_14" \
    --zone "asia-southeast1-b" \
    --network "dev" --no-assign-ip \
    --tier "db-f1-micro" \
    --availability-type "zonal" \
    --storage-auto-increase --storage-size 10 --storage-type "SSD" \
    --insights-config-query-insights-enabled \
    --insights-config-record-application-tags \
    --insights-config-record-client-address \
    --insights-config-query-string-length 1024 \
    --maintenance-release-channel "preview" \
    --maintenance-window-day "SUN" --maintenance-window-hour 20 \
    --backup --backup-location "asia" --backup-start-time "16:00" \
    --enable-password-policy --password-policy-min-length 8 \
    --password-policy-complexity "COMPLEXITY_DEFAULT" \
    --password-policy-disallow-username-substring
gcloud sql databases create api --instance "dev-postgres"
gcloud sql databases create datamart --instance "dev-postgres"
```

Change the root password.

```sh
gcloud sql users set-password postgres \
    --instance "dev-postgres" \
    --prompt-for-password
```

Save DB configurations as a secret.

```sh
cat << EOF > postgres-secrets.json
{
  "DB_HOST": "$( gcloud sql instances describe dev-postgres --format 'value(ipAddresses[0].ipAddress)' ):5432",
  "DB_NAME": "api",
  "DB_USER": "postgres",
  "DB_PASSWORD": "<your-root-password>"
}
EOF
gcloud secrets create dev-postgres-secrets --replication-policy "automatic" \
    --data-file postgres-secrets.json
rm -f postgres-secrets.json
```

### PostgreSQL Proxy

Create a service account to access the database.

```sh
export project_id=$( gcloud config get-value project )
gcloud iam service-accounts create db-client \
    --display-name "Cloud SQL Client SA" \
    --description "Service Account for Cloud SQL client"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:db-client@${project_id}.iam.gserviceaccount.com" \
    --role "roles/cloudsql.client"
```

Create a bastion server.

```sh
gcloud compute instances create dev-bastion \
    --zone "asia-southeast1-b" --machine-type "f1-micro" \
    --network-interface "network-tier=PREMIUM,subnet=dev-singapore" \
    --service-account "db-client@${project_id}.iam.gserviceaccount.com" \
    --scopes "https://www.googleapis.com/auth/cloud-platform" \
    --metadata=enable-oslogin=true,startup-script='rm -f proxy
curl -so proxy https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 && chmod +x proxy
./proxy -instances='$(gcloud sql instances describe dev-postgres --format "value(connectionName)")'=tcp:0.0.0.0:5432'
```

SSH Tunneling for the private PostgreSQL instance

```sh
gcloud compute ssh dev-bastion --zone "asia-southeast1-b" \
    --tunnel-through-iap -- -N -L 5432:localhost:5432
```

Access the DB locally :)

```sh
psql "host=127.0.0.1 sslmode=disable dbname=api user=postgres"
api=> \l
api=> CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
api=> CREATE TABLE objects (
  id         UUID         NOT NULL DEFAULT uuid_generate_v4(),
  uri        VARCHAR(256) NOT NULL,
  length     INT          NOT NULL DEFAULT 0,
  publish_at TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT objects_pkey PRIMARY KEY (id)
);
api=> \d
api=> \q
```

### Message subscriber (Cloud Run)

Create a service account to access the database.

```sh
gcloud iam service-accounts create dev-subscriber \
    --display-name "Dev subscriber's SA" \
    --description "Service Account for Secret Manager & Cloud SQL client"
gcloud secrets add-iam-policy-binding dev-postgres-secrets \
    --member "serviceAccount:dev-subscriber@${project_id}.iam.gserviceaccount.com" \
    --role roles/secretmanager.secretAccessor
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:dev-subscriber@${project_id}.iam.gserviceaccount.com" \
    --role "roles/cloudsql.client"
```

Deploy a service on Cloud Run.

```sh
gcloud run deploy dev-subscriber --image gcr.io/cloudrun/hello --region "asia-southeast1" \
    --platform "managed" --cpu 1.0 --memory 128Mi --concurrency 5 --no-allow-unauthenticated
export dev_subscriber_url="$( gcloud run services describe dev-subscriber \
        --region asia-southeast1 --format 'value(status.address.url)')"
gcloud run deploy dev-subscriber --source subscriber --region "asia-southeast1" \
    --service-account "dev-subscriber@${project_id}.iam.gserviceaccount.com" \
    --update-secrets CLOUD_SQL_CREDENTIALS_SECRET=dev-postgres-secrets:latest \
    --set-env-vars "TARGET_URL=${dev_subscriber_url}" \
    --vpc-connector "dev-vpc-access"
```

Create a SA for tests.

```sh
gcloud iam service-accounts create dev-subscriber-invoker \
    --display-name "Dev subscriber invoker SA" \
    --description "Service Account for invoking dev-subscriber"
gcloud run services add-iam-policy-binding dev-subscriber --region "asia-southeast1" \
    --member "serviceAccount:dev-subscriber-invoker@${project_id}.iam.gserviceaccount.com" \
    --role "roles/run.invoker"
```

Run test requests from local.

```sh
gcloud iam service-accounts add-iam-policy-binding \
    "dev-subscriber-invoker@${project_id}.iam.gserviceaccount.com" \
    --member "user:$( gcloud config get-value core/account )" \
    --role "roles/iam.serviceAccountTokenCreator"
token=$( gcloud auth print-identity-token \
    --impersonate-service-account dev-subscriber-invoker@${project_id}.iam.gserviceaccount.com \
    --audiences "${dev_subscriber_url}" )
curl -isH "Authorization: Bearer ${token}" "${dev_subscriber_url}"
curl -isH "Authorization: Bearer ${token}" -H "Content-Type: application/json" \
    --data @subscriber/sample-message.json "${dev_subscriber_url}/messages"
```

### Pub/Sub

```sh
gcloud pubsub topics create dev --message-storage-policy-allowed-regions "asia-southeast1"
export project_number=$(gcloud projects describe ${project_id} --format="value(projectNumber)")
gcloud iam service-accounts add-iam-policy-binding \
    "dev-subscriber-invoker@${project_id}.iam.gserviceaccount.com" \
    --member "serviceAccount:service-${project_number}@gcp-sa-pubsub.iam.gserviceaccount.com" \
    --role "roles/iam.serviceAccountTokenCreator"
gcloud pubsub subscriptions create dev-subscriptions --topic "dev" \
    --push-endpoint "${dev_subscriber_url}/messages" \
    --push-auth-token-audience "${dev_subscriber_url}" \
    --push-auth-service-account "dev-subscriber-invoker@${project_id}.iam.gserviceaccount.com" \
    --ack-deadline 60 --min-retry-delay 10s --max-retry-delay 600s \
    --message-retention-duration 3d
```

### CI/CD pipelines

```sh
gcloud iam service-accounts create service-deployer \
    --display-name "Service deployment SA" \
    --description "Service Account for deployinh Cloud Run services"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:service-deployer@${project_id}.iam.gserviceaccount.com" \
    --role "roles/run.admin"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:service-deployer@${project_id}.iam.gserviceaccount.com" \
    --role "roles/storage.admin"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:service-deployer@${project_id}.iam.gserviceaccount.com" \
    --role "roles/artifactregistry.writer"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:service-deployer@${project_id}.iam.gserviceaccount.com" \
    --role "roles/cloudbuild.builds.editor"
gcloud projects add-iam-policy-binding "${project_id}" \
    --member "serviceAccount:service-deployer@${project_id}.iam.gserviceaccount.com" \
    --role "roles/iam.serviceAccountUser"
gcloud iam service-accounts keys create key.json \
    --iam-account "service-deployer@${project_id}.iam.gserviceaccount.com"
cat key.json && rm -f key.json
```

## Local Development

Install [Poetry](https://python-poetry.org/docs/).

```sh
poetry self add poetry-dotenv-plugin
```

Login Google Cloud.

```sh
gcloud auth application-default login
```

### Pub/Sub subscriber

```sh
cd subscriber
cat << EOF > .env
DB_USER=postgres
DB_PASSWORD=<your-root-password>
DB_NAME=api
DB_HOST=localhost:5432
EOF
poetry install
poetry run uvicorn --host 0.0.0.0 --port 8080 main:app --reload
```

```sh
curl -i --data @sample-message.json localhost:8080/messages
```

Test source code.

```sh
poetry run flake8 --exclude=.venv .
poetry run black .
poetry run pip-audit
```

### Pub/Sub publisher

```sh
cd publisher
poetry install
poetry run python main.py -p "${project_id}" -t dev test
```
