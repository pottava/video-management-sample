import argparse
import os

from google.cloud import pubsub_v1

parser = argparse.ArgumentParser()
parser.add_argument("data")
parser.add_argument("-p", default=os.environ.get("PROJECT_ID", None))
parser.add_argument("-t", default=os.environ.get("TOPIC_ID", None))
args = parser.parse_args()

try:
    publisher = pubsub_v1.PublisherClient()
    topic = publisher.topic_path(args.p, args.t)
    publisher.publish(topic, args.data.encode("utf-8"))
    print(f"Published a message to {topic}.")
except Exception as e:
    print(e)
